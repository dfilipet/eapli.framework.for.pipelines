/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.model.general;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Paulo Gandra Sousa
 *
 */
public class EmailAddressTest {

    /**
     * Test method for
     * {@link eapli.framework.domain.model.general.EmailAddress#EmailAddress(java.lang.String)}.
     */
    @Test
    public void testEmailAddress() {
        Assert.assertNotNull(new EmailAddress("geral@acme.com"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmailAddressHasAt() {
        new EmailAddress("geralATacme.com");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmailAddressHasHostAndDomain() {
        new EmailAddress("geral@acme");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmailAddressHasName() {
        new EmailAddress("@acme.com");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmailAddressIsNotNull() {
        new EmailAddress(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmailAddressIsNotEmpty() {
        new EmailAddress("");
    }
}
