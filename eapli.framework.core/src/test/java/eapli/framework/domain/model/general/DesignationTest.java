/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.model.general;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author sou03408
 *
 */
public class DesignationTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureNotNull() {
        Designation.valueOf(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNotEmpty() {
        Designation.valueOf("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNoHeadingWhitespace() {
        Designation.valueOf("  ABC");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNoTrailingWhitespace() {
        Designation.valueOf("ABC   ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNoHeadingNorTrailingWhitespace() {
        Designation.valueOf("  ABC  ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNoWhitespaceOnly() {
        Designation.valueOf("    ");
    }

    @Test
    public void ensureStartsWithSomething() {
        Assert.assertNotNull(Designation.valueOf("ABC"));
    }
}
