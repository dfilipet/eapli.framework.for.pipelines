/**
 * Provides a generic base class for implementing repositories using JPA. this
 * class is compatible with the Repository interface defined in
 * {@link eapli.framework.domain.repositories.DomainRepository}
 *
 * @author Paulo Gandra Sousa
 *
 */
package eapli.framework.infrastructure.repositories.impl.jpa;
