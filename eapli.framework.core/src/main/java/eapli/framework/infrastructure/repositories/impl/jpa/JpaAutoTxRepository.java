/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.infrastructure.repositories.impl.jpa;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.TypedQuery;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.Repository;
import eapli.framework.validations.Preconditions;

/**
 * A JPA repository implementation for use with applications not running inside
 * a container. this class can either support working within a
 * TransactionalContext or a single immediate transaction mode. if running in
 * single immediate mode a transaction is initiated, committed and the
 * connection closed in the scope of each repository method call.
 *
 * @param <T>
 *            the entity type that we want to build a repository for
 * @param <K>
 *            the primary key type of the table
 * @param <I>
 *            the type of the <b>business identity</b> of the entity
 *
 * @author Paulo Gandra Sousa
 */
public class JpaAutoTxRepository<T, K, I> implements Repository<T, K> {

    protected final JpaBaseRepository<T, K, I> repo;
    private final TransactionalContext autoTx;

    @SuppressWarnings("rawtypes")
    public JpaAutoTxRepository(final String persistenceUnitName, final String identityFieldName) {
        this(persistenceUnitName, new HashMap(), identityFieldName);
    }

    public JpaAutoTxRepository(final String persistenceUnitName, @SuppressWarnings("rawtypes") final Map properties,
            final String identityFieldName) {
        final ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        @SuppressWarnings("unchecked")
        final Class<T> entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];

        this.repo = new JpaTransactionalRepository<>(persistenceUnitName, properties, entityClass, identityFieldName);
        this.autoTx = null;
    }

    public JpaAutoTxRepository(final TransactionalContext autoTx, final String identityFieldName) {
        Preconditions.nonNull(autoTx);

        final ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();

        @SuppressWarnings("unchecked")
        final Class<T> entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];

        this.repo = new JpaWithTransactionalContextRepository<>(autoTx, entityClass, identityFieldName);
        this.autoTx = autoTx;
    }

    public static TransactionalContext buildTransactionalContext(final String persistenceUnitName) {
        return new JpaTransactionalContext(persistenceUnitName);
    }

    public static TransactionalContext buildTransactionalContext(final String persistenceUnitName,
            @SuppressWarnings("rawtypes") final Map properties) {
        return new JpaTransactionalContext(persistenceUnitName, properties);
    }

    public TransactionalContext context() {
        return this.autoTx;
    }

    /**
     * returns if the repository is running in single transaction mode or within
     * a TransactionalContext
     *
     * @return true if the repository is running in single transaction mode
     *         false if running within a Transactional Context
     */
    public boolean isInTransaction() {
        return context() == null;
    }

    @Override
    public void delete(final T entity) {
        this.repo.delete(entity);
    }

    @Override
    public void deleteById(final K id) {
        this.repo.deleteById(id);
    }

    public void deleteOfIdentity(final I entityId) {
        this.repo.deleteOfIdentity(entityId);
    }

    @Override
    public <S extends T> S save(final S entity) {
        return this.repo.save(entity);
    }

    @Override
    public Iterable<T> findAll() {
        return this.repo.findAll();
    }

    @Override
    public Optional<T> findById(final K id) {
        return this.repo.findById(id);
    }

    public Optional<T> ofIdentity(final I id) {
        return this.repo.ofIdentity(id);
    }

    @Override
    public long count() {
        return this.repo.count();
    }

    public long size() {
        return count();
    }

    public Iterator<T> iterator() {
        return this.repo.iterator();
    }

    protected List<T> match(final String where) {
        return repo.match(where);
    }

    protected List<T> match(final String whereWithParameters, final Map<String, Object> params) {
        return repo.match(whereWithParameters, params);
    }

    protected List<T> match(final String where, final Object... args) {
        return repo.match(where, args);
    }

    protected Optional<T> matchOne(final String where) {
        return repo.matchOne(where);
    }

    protected Optional<T> matchOne(final String whereWithParameters, final Map<String, Object> params) {
        return repo.matchOne(whereWithParameters, params);
    }

    protected Optional<T> matchOne(final String where, final Object... args) {
        return repo.matchOne(where, args);
    }

    protected <U> TypedQuery<U> createQuery(final String queryString, final Class<U> class1) {
        return repo.createQuery(queryString, class1);
    }
}
