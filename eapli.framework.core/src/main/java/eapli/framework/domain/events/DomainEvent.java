/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.events;

import java.io.Serializable;
import java.util.Calendar;

import eapli.framework.domain.model.ValueObject;

/**
 * A Domain event is a domain/driven design pattern to signal significant events
 * that happened in the domain.
 *
 * these are not technical issues/events but meaningful domain concepts
 *
 * @author Paulo Gandra Sousa
 */
public interface DomainEvent extends ValueObject, Serializable {

    /**
     * returns the date and time when the event occurred in the domain
     *
     * @return
     */
    Calendar occurredAt();

    /**
     * returns the date and time when the event was recorded in the system
     *
     * @return
     */
    Calendar registeredAt();
}
