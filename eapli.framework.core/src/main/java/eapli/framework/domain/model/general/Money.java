/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.model.general;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Currency;
import java.util.stream.Collector;

import javax.persistence.Embeddable;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

/**
 * represents money values.
 *
 * based on http://martinfowler.com/eaaDev/quantity.html
 *
 */
@Embeddable
public class Money implements Comparable<Money>, Serializable, ValueObject {

    private static final long serialVersionUID = 1L;
    private static final transient int HUNDRED = 100;
    // @Convert(converter = MoneyAmountConverter.class)
    private final BigInteger amount;
    private final Currency currency;

    /**
     * For ORM tool only
     */
    protected Money() {
        amount = null;
        currency = null;
    }

    /**
     * Constructs a new Money object.
     *
     * You'll notice there are no setters. Money is a Value Object and is thus
     * immutable. It helps to have a variety of constructors to make it easy to
     * make monies. Constructors that convert from basic numeric types are
     * always useful.
     *
     * @param amount
     *            the amount in the units of currency, e.g., 100.50 EUR
     * @param currency
     */
    public Money(final double amount, final Currency currency) {
        this.amount = BigInteger.valueOf(Math.round(amount * HUNDRED));
        this.currency = currency;
    }

    public Money(final long amount, final Currency currency) {
        this.amount = BigInteger.valueOf(amount * HUNDRED);
        this.currency = currency;
    }

    /**
     * special constructor to reconstruct objects from the database - to be used
     * in SELECT NEW JPQL queries. this is an example of a "public but not
     * published" interface
     *
     * @TODO check other ways to avoid having this special constructor
     * @param amount
     * @param currency
     */
    public Money(final BigInteger amount, final String currency) {
        this.amount = amount;
        this.currency = Currency.getInstance(currency);
    }

    /**
     *
     * @param amountInCents
     * @param currency
     */
    private Money(final BigInteger amountInCents, final Currency currency) {
        assert amountInCents != null;
        assert currency != null;

        amount = amountInCents;
        this.currency = currency;
    }

    /**
     * If you use one currency a lot, you may want a special constructor for
     * that currency.
     */
    public static Money dollars(final double amount) {
        return new Money(amount, Currency.getInstance("USD"));
    }

    /**
     * If you use one currency a lot, you may want a special constructor for
     * that currency.
     */
    public static Money euros(final double amount) {
        return new Money(amount, Currency.getInstance("EUR"));
    }

    /**
     * parses a string representation of a money, e.g., "123.50 EUR"
     *
     * @param arg0
     * @return
     */
    public static Money valueOf(final String arg0) {
        final String[] parts = arg0.split(" ");
        Preconditions.areEqual(parts.length, 2);

        final double am = Double.parseDouble(parts[0]);
        final Currency curr = Currency.getInstance(parts[1]);
        return new Money(am, curr);
    }

    /**
     * creates a collector that can be used to sum up a stream of Money objects
     * in a reduction style. that is, since Money is an immutable object,
     * collecting Money objects from streams have the implication of creating a
     * new Money object for each collect step which might impact performance.
     *
     * alternatively the collector returned could use an internal mutable
     * representation of Money and just create an instance when combining
     * streams
     *
     * @return
     */
    public static Collector<Money, MoneyCollector, Money> collector(final Money zero) {
        Preconditions.nonNull(zero);

        return Collector.of(() -> new MoneyCollector(zero), (a, e) -> a.add(e), (a1, a2) -> a1.combine(a2),
                a -> a.current);
    }

    private static class MoneyCollector {
        private Money current;

        public MoneyCollector(final Money zero) {
            current = zero;
        }

        public void add(final Money b) {
            current = current.add(b);
        }

        public MoneyCollector combine(final MoneyCollector other) {
            current = current.add(other.current);
            return this;
        }
    }

    /**
     * Returns the amount portion of this Money object.
     *
     * Notice that I use a BigInteger. In Java I could equally well use a
     * BigDecimal, but in many languages an integer is the only decent option,
     * so using an integer seems the best for explanation. Don't be afraid to
     * choose your representation of the amount part of a quantity based on
     * performance factors. The beauty of objects is that you can choose any
     * data structure you like on the inside, providing you hide it on the
     * outside.
     */
    public double amount() {
        return amount.doubleValue() / HUNDRED;
    }

    public BigDecimal amountAsDecimal() {
        return BigDecimal.valueOf(amount());
    }

    /**
     * Returns the currency of this Money object.
     *
     * @return
     */
    public Currency currency() {
        return currency;
    }

    /**
     * Adds two Money objects and returns the result as a new object.
     *
     * For addition and subtraction I'm not trying to do any fancy conversion.
     * Notice that I'm using a special constructor with a marker argument.
     */
    public Money add(final Money arg) {
        Preconditions.ensure(() -> currency.equals(arg.currency), "Cannot add different currencies");

        return new Money(amount.add(arg.amount), currency);
    }

    /**
     * Subtracts two Money instances and returns a third one with the result.
     *
     * @param arg
     * @return
     */
    public Money subtract(final Money arg) {
        return add(arg.negate());
    }

    public Money negate() {
        return new Money(amount.negate(), currency);
    }

    /**
     * Multiplies two Money objects and returns the result a new object.
     *
     */
    public Money multiply(final double arg) {
        return new Money(amount() * arg, currency);
    }

    /**
     * Divides this Money object by a certain integer denominator doing the
     * right allocation of cents.
     *
     * Multiplication is very straightforward. But division is not, as we have
     * to take care of errant pennies. We'll do that by returning an array of
     * monies, such that the sum of the array is equal to the original amount,
     * and the original amount is distributed fairly between the elements of the
     * array. Fairly in this sense means those at the begining get the extra
     * pennies.
     */
    public Money[] divide(final int denominator) {
        final BigInteger bigDenominator = BigInteger.valueOf(denominator);
        final Money[] result = new Money[denominator];
        final BigInteger simpleResult = amount.divide(bigDenominator);
        for (int i = 0; i < denominator; i++) {
            result[i] = new Money(simpleResult, currency);
        }
        final int remainder = amount.subtract(simpleResult.multiply(bigDenominator)).intValue();
        for (int i = 0; i < remainder; i++) {
            result[i] = result[i].add(new Money(BigInteger.valueOf(1), currency));
        }
        return result;
    }

    /**
     * Compares two Money objects.
     *
     * Next we'll look at comparing monies, in Java the approach is to implement
     * comparable.
     */
    @Override
    public int compareTo(final Money arg) {
        Preconditions.areEqual(currency, arg.currency, "Cannot add different currencies");

        return amount.compareTo(arg.amount);
    }

    /**
     * Compares two Money objects.
     *
     * It's also useful to provide some better named operations such as: That
     * makes methods that need the comparison much easier to read.
     */
    public boolean greaterThan(final Money arg) {
        return compareTo(arg) > 0;
    }

    /**
     * Compares two Money objects.
     */
    public boolean lessThan(final Money arg) {
        return compareTo(arg) < 0;
    }

    /**
     * Compares two Money objects.
     */
    public boolean greaterThanOrEqual(final Money arg) {
        return compareTo(arg) >= 0;
    }

    /**
     * Compares two Money objects.
     */
    public boolean lessThanOrEqual(final Money arg) {
        return compareTo(arg) <= 0;
    }

    /**
     * Since money is a value, it should override equals.
     */
    @Override
    public boolean equals(final Object arg) {
        if (!(arg instanceof Money)) {
            return false;
        }
        final Money other = (Money) arg;
        return currency.equals(other.currency) && amount.equals(other.amount);
    }

    /**
     * Since you override equals, don't forget to also override hash (here's a
     * simple suggestion for that).
     */
    @Override
    public int hashCode() {
        int result = 11;
        result = 37 * result + amount.hashCode();
        result = 37 * result + currency.hashCode();
        return result;
    }

    public int signum() {
        return amount.signum();
    }

    @Override
    public String toString() {
        return BigDecimal.valueOf(amount()) + " " + currency();
    }
}
