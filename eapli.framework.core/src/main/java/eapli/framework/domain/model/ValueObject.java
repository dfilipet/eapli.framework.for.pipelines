/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.model;

import java.io.Serializable;

/**
 * A value object is a Domain-Driven Design pattern for domain concepts which do
 * not have a thread of continuity neither need to be tracked by their identity
 * but for the value of its attributes. These are <b>immutable</b> objects which
 * can be freely shared and discarded and replaced by another instance. Equality
 * is done thru comparison of the attributes values.
 *
 * Typical examples are:
 * <p>
 * - Address (in most scenarios)
 * <p>
 * - Color
 * <p>
 * - CustomerNumber
 * <p>
 * - Money
 *
 * Make sure you don't provide mutator methods, always construct the object in a
 * valid state and all your private fields are marked final (If you are using
 * ORM tools you might need a default constructor in which case you won't be
 * able to make the fields final)
 *
 * check the Java Time API naming conventions for an excellent example on how to
 * name methods of a Value Object
 * https://docs.oracle.com/javase/tutorial/datetime/overview/naming.html
 *
 *
 * @author Paulo Gandra Sousa
 */
public interface ValueObject extends Serializable {

    /**
     * returns a representation of this value object as a String.
     *
     * @return
     */
    @Override
    String toString();

    /**
     * Value objects are compared by the values of its properties.
     *
     * see, for instance, <a href=
     * "https://www.sitepoint.com/implement-javas-equals-method-correctly/">https://www.sitepoint.com/implement-javas-equals-method-correctly/</a>
     *
     * @param other
     * @return
     */
    @Override
    public boolean equals(Object other);

    /**
     * hash code of this object according to java rules. i.e., the same fields used
     * in equals() should be used in hashCode().
     *
     * see <a href=
     * "http://stackoverflow.com/questions/113511/best-implementation-for-hashcode-method">
     * stack overflow</a> for a nice discussion about hashCode() and equals()
     *
     * @return
     */
    @Override
    public int hashCode();
}
