/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.domain.model.domains;

import java.io.Serializable;

import javax.persistence.Embeddable;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.Factory;
import eapli.framework.validations.Invariants;

/**
 * a generic immutable interval class, i.e., a continuous domain.
 *
 * @author Paulo Gandra Sousa
 * @param <T>
 */
@Embeddable
public class Interval<T extends Serializable & Comparable<T>> implements ValueObject {

    private static final long serialVersionUID = 1L;

    protected enum BoundaryLimitType {
        INFINITY, OPEN, CLOSED
    }

    private final T start;
    private final T end;
    private final BoundaryLimitType startBoundary;
    private final BoundaryLimitType endBoundary;

    /**
     * The builder of intervals
     *
     * @author Paulo Gandra Sousa
     *
     * @param <R>
     */
    public static class IntervalBuilder<R extends Comparable<R> & Serializable> implements Factory<Interval<R>> {
        private final R start;
        private R end;
        private final BoundaryLimitType startBoundary;
        private BoundaryLimitType endBoundary;

        /**
         * starts building a range at start
         *
         * @param start
         * @param startBoundary
         */
        private IntervalBuilder(final R start, final BoundaryLimitType startBoundary) {
            assert (startBoundary == BoundaryLimitType.INFINITY && start == null)
                    || (startBoundary != BoundaryLimitType.INFINITY && start != null);
            this.start = start;
            this.startBoundary = startBoundary;
        }

        public IntervalBuilder<R> closedTo(final R anchor) {
            this.end = anchor;
            this.endBoundary = BoundaryLimitType.CLOSED;
            return this;
        }

        public IntervalBuilder<R> openTo(final R anchor) {
            this.end = anchor;
            this.endBoundary = BoundaryLimitType.OPEN;
            return this;
        }

        public IntervalBuilder<R> toInfinity() {
            this.end = null;
            this.endBoundary = BoundaryLimitType.INFINITY;
            return this;
        }

        @Override
        public Interval<R> build() {
            return new Interval<>(this.start, this.startBoundary, this.end, this.endBoundary);
        }
    }

    protected Interval() {
        // for ORM
        start = end = null;
        endBoundary = startBoundary = null;
    }

    /**
     * special private constructor for empty intervals
     *
     * @param b
     *            marker parameter to enable the compiler to distinguish the
     *            constructors
     */
    @SuppressWarnings("squid:S1172")
    private Interval(final boolean b) {
        start = end = null;
        endBoundary = startBoundary = null;
    }

    /**
     * constructs an interval.
     *
     * @param start
     *            anchor start of the interval or null to represent infinity
     * @param end
     *            anchor end of the interval or null to represent infinity
     * @param startBoundary
     *            indicates if the interval is open or closed at the start anchor
     * @param endBoundary
     *            indicates if the interval is open or closed at the end anchor
     */
    protected Interval(final T start, final BoundaryLimitType startBoundary, final T end,
            final BoundaryLimitType endBoundary) {
        if ((start == null && startBoundary != BoundaryLimitType.INFINITY)
                || (end == null && endBoundary != BoundaryLimitType.INFINITY)) {
            throw new IllegalArgumentException("start or end must be non-null");
        }

        if (end != null && start != null && end.compareTo(start) < 0) {
            throw new IllegalArgumentException("The end value of a range must be bigger than its start");
        }
        if (end != null && start != null && end.compareTo(start) == 0
                && (startBoundary == BoundaryLimitType.OPEN || endBoundary == BoundaryLimitType.OPEN)) {
            throw new IllegalArgumentException("An empty range is not allowed");
        }

        this.start = start;
        this.end = end;
        this.startBoundary = startBoundary;
        this.endBoundary = endBoundary;
    }

    /**
     * A factory method for intervals that start at "negative infinity"
     *
     * @return a builder
     */
    public static <T extends Comparable<T> & Serializable> IntervalBuilder<T> fromInfinity() {
        return new IntervalBuilder<>(null, BoundaryLimitType.INFINITY);
    }

    /**
     * A factory method for closed intervals that start at a specific anchor point
     *
     * @return a builder
     */
    public static <T extends Comparable<T> & Serializable> IntervalBuilder<T> closedFrom(final T start) {
        return new IntervalBuilder<>(start, BoundaryLimitType.CLOSED);
    }

    /**
     * A factory method for open intervals that start at a specific anchor point
     *
     * @return a builder
     */
    public static <T extends Comparable<T> & Serializable> IntervalBuilder<T> openFrom(final T start) {
        return new IntervalBuilder<>(start, BoundaryLimitType.OPEN);
    }

    @SuppressWarnings("rawtypes")
    private static final Interval EMPTY = new Interval(true);

    /**
     * returns an empty interval
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T> & Serializable> Interval<T> empty() {
        return EMPTY;
    }

    public boolean isEmpty() {
        return start == end && start == null && endBoundary == startBoundary && endBoundary == null;
    }

    private boolean notEmpty() {
        return !isEmpty();
    }

    /**
     * checks if a value belongs in this interval
     *
     * @param target
     * @return
     */
    public boolean includes(final T target) {
        if (isEmpty()) {
            return false;
        } else if (this.startBoundary != BoundaryLimitType.INFINITY && this.endBoundary != BoundaryLimitType.INFINITY) {
            return includesInFiniteRanges(target);
        } else if (this.endBoundary == BoundaryLimitType.INFINITY) {
            return includesInToInfinityRanges(target);
        } else {
            assert this.startBoundary == BoundaryLimitType.INFINITY;
            return includesInFromInfinityRanges(target);
        }
    }

    private boolean includesInFromInfinityRanges(final T target) {
        if (target.compareTo(this.end) > 0) {
            return false;
        }
        return !(hasOpenEnd() && target.compareTo(this.end) == 0);
    }

    private boolean includesInToInfinityRanges(final T target) {
        if (target.compareTo(this.start) < 0) {
            return false;
        }
        return !(hasOpenStart() && target.compareTo(this.start) == 0);
    }

    private boolean includesInFiniteRanges(final T target) {
        if (target.compareTo(this.start) < 0 || target.compareTo(this.end) > 0) {
            return false;
        }
        if (hasOpenStart() && target.compareTo(this.start) == 0) {
            return false;
        }
        return !(hasOpenEnd() && target.compareTo(this.end) == 0);
    }

    public boolean isToInfinity() {
        return this.endBoundary == BoundaryLimitType.INFINITY;
    }

    public boolean isFromInfinity() {
        return this.startBoundary == BoundaryLimitType.INFINITY;
    }

    public T start() {
        Invariants.ensure(this::notEmpty);

        return this.start;
    }

    public T end() {
        Invariants.ensure(this::notEmpty);

        return this.end;
    }

    /**
     * checks if this interval intersects another interval
     *
     * @param other
     * @return
     */
    public boolean intersects(final Interval<T> other) {
        return !this.intersection(other).equals(empty());
    }

    /**
     * returns the interval consisting of the intersection of this interval and
     * another one
     *
     * @param other
     * @return
     */
    public Interval<T> intersection(final Interval<T> other) {
        throw new UnsupportedOperationException("Not yet developed");
    }

    /**
     * returns a new interval by extending the start of this one. the
     * openness/closeness of the start boundary as well as the end will be the same
     * of the original interval
     *
     * @param other
     * @return
     */
    public Interval<T> withStart(final T newStart) {
        return new Interval<>(newStart, this.startBoundary, this.end, this.endBoundary);
    }

    /**
     * returns a new interval by extending the end of this one. the
     * openness/closeness of the end boundary as well as the start will be the same
     * of the original interval
     *
     * @param other
     * @return
     */
    public Interval<T> withEnd(final T newEnd) {
        return new Interval<>(this.start, this.startBoundary, newEnd, this.endBoundary);
    }

    /**
     * checks if this interval overlaps another one, that is, if this interval fully
     * contains the other one.
     *
     * for instance
     *
     * A = [2..5]
     *
     * B = [0..10]
     *
     * C = ]2..4]
     *
     * D = [8..10]
     *
     * A.overlaps(C) == true
     *
     * A.overlaps(B) == false
     *
     * B.overlaps(A) == true
     *
     * C.overlaps(D) == false
     *
     * D.overlaps(C) == false
     *
     * B.overlaps(D) == true
     *
     * @param other
     * @return
     */
    public boolean overlaps(final Interval<T> other) {
        throw new UnsupportedOperationException("Not yet developed");
    }

    public boolean hasOpenEnd() {
        return this.endBoundary == BoundaryLimitType.OPEN || endBoundary == BoundaryLimitType.INFINITY;
    }

    public boolean hasOpenStart() {
        return this.startBoundary == BoundaryLimitType.OPEN || startBoundary == BoundaryLimitType.INFINITY;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "[]";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(startBracket(hasOpenStart()));
        sb.append(rangeValue(this.isFromInfinity(), this.start));
        sb.append(", ");
        sb.append(rangeValue(this.isToInfinity(), this.end));
        sb.append(endBracket(hasOpenEnd()));
        return sb.toString();
    }

    private char startBracket(final boolean isOpen) {
        if (isOpen) {
            return ']';
        } else {
            return '[';
        }
    }

    private char endBracket(final boolean isOpen) {
        if (isOpen) {
            return '[';
        } else {
            return ']';
        }
    }

    private String rangeValue(final boolean isInfinity, final T value) {
        if (isInfinity) {
            return "oo";
        } else {
            return value.toString();
        }
    }
}
