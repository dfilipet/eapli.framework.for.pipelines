/*
 * MIT License
 *
 * Copyright (c) 2013-2019 Paulo Gandra de Sousa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.framework.infrastructure.authz.domain.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import eapli.framework.domain.model.general.EmailAddress;
import eapli.framework.util.Calendars;

/**
 *
 * @author sou03408
 *
 */
public class SystemUserTest {
    private static final String PASSWORD1 = "Password1";
    private SystemUser instance;
    private Calendar dateOfCreation;

    @Before
    public void setUp() throws Exception {
        dateOfCreation = Calendars.now();
        instance = new SystemUserBuilder().withUsername("abc").withPassword(PASSWORD1).withFirstName("Ant")
                .withLastName("Boocam").withEmail("a@b.com").createdOn(dateOfCreation).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveUsername() {
        final Set<Role> roles = new HashSet<>();
        roles.add(Role.valueOf("R"));
        new SystemUser(null, Password.valueOf(PASSWORD1), Name.valueOf("Ant", "Silva"), EmailAddress.valueOf("a@b.co"),
                roles);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHavePassword() {
        final Set<Role> roles = new HashSet<>();
        roles.add(Role.valueOf("R"));
        new SystemUser(Username.valueOf("username"), null, Name.valueOf("Ant", "Silva"), EmailAddress.valueOf("a@b.co"),
                roles);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveName() {
        final Set<Role> roles = new HashSet<>();
        roles.add(Role.valueOf("R"));
        new SystemUser(Username.valueOf("username"), Password.valueOf(PASSWORD1), null, EmailAddress.valueOf("a@b.co"),
                roles);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveEmail() {
        final Set<Role> roles = new HashSet<>();
        roles.add(Role.valueOf("R"));
        new SystemUser(Username.valueOf("username"), Password.valueOf(PASSWORD1), Name.valueOf("Ant", "Silva"), null,
                roles);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveDateOfCreation() {
        final Set<Role> roles = new HashSet<>();
        roles.add(Role.valueOf("R"));
        new SystemUser(Username.valueOf("username"), Password.valueOf(PASSWORD1), Name.valueOf("Ant", "Silva"),
                EmailAddress.valueOf("a@b.co"), roles, null);
    }

    @Test
    public void ensureDateOfCreationIsSameAsCreatedOn() {
        assertEquals(dateOfCreation, instance.createdOn());
    }

    @Test
    public void ensureHasCreatedOn() {
        assertNotNull(instance.createdOn());
    }

    @Test
    public void ensurePasswordMatches() {
        assertTrue(instance.passwordMatches(Password.valueOf(PASSWORD1)));
    }

    @Test
    public void ensurePasswordDoesntMatch() {
        assertFalse(instance.passwordMatches(Password.valueOf(PASSWORD1 + "xpto")));
    }
}
